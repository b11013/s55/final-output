//CONTEXT API
//passing info from one info to another w/o using props

import React from "react";

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;
export default UserContext;
