import { Form, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react"; //to store value from inputfields
import UserContext from "../UserContext";
import { Navigate, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

// form handling
export default function Register(props) {
  const [email, setEmail] = useState("");

  const history = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");

  const [password, setPassword] = useState("");
  // const [password1, setPassword1] = useState("");
  const [isActive, setIsActive] = useState(false);

  const { user, setUser } = useContext(UserContext);

  console.log(user);

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNo.length === 11 &&
      email !== "" &&
      password !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNo, email, password]);

  function registerUser(e) {
    e.preventDefault();
    fetch("http://localhost:4000/users/checkEmailExists", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "info",
            text: "Please provide another email",
          });
        } else {
          fetch("http://localhost:4000/users/", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data.email) {
                Swal.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Thank you for registering",
                });
                history("/login");
              } else {
                Swal.fire({
                  title: "Registration failed",
                  icon: "error",
                  text: "Something went wrong",
                });
              }
            });
        }
      });

    // localStorage.setItem("email", email);

    // setUser({
    //   email: localStorage.getItem("email"),
    // });

    setFirstName("");
    setLastName("");
    setMobileNo("");

    setEmail("");
    setPassword("");

    // setPassword1("");
    //alert("Thank you for registering " + email);
  }

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <div>
      <h1 className="mt-4">Register Here</h1>
      <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group controlId="firstName">
          <Form.Label className="mt-2">First Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Please input your first name here."
            required
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="lastName">
          <Form.Label className="mt-2">Last Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Please input your last name here."
            required
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="mobileNo">
          <Form.Label className="mt-2">Mobile Number:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Please input your 11-digit mobile number here."
            required
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="userEmail">
          <Form.Label className="mt-2">Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Please input your email here."
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className="text-muted">
            {" "}
            We'll never share your email with anyone else. 😏
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="password">
          <Form.Label className="mt-2">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Please input your password here."
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        {/* <Form.Group controlId="password2">
          <Form.Label className="mt-2">Verify Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Please verify your password here."
            required
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
          />
        </Form.Group> */}

        <p>
          Already registered? <Link to="/login">Log In Here</Link>
        </p>

        {isActive ? (
          <Button
            variant="success"
            type="submit"
            id="submitBTN"
            className="mt-3 mb-5"
          >
            Register
          </Button>
        ) : (
          <Button
            variant="danger"
            type="submit"
            id="submitBTN"
            className="mt-3 mb-5"
            disabled
          >
            Register
          </Button>
        )}
      </Form>
    </div>
  );
}
