import { Form, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login(props) {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  //   const loginSuccess = () => {
  //     return alert(email + " has been verified. Welcome back!");
  //   };

  function logInUser(e) {
    e.preventDefault();

    fetch("http://localhost:4000/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (typeof data.accessToken != "undefined") {
          localStorage.setItem("token", data.accessToken);

          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Booking App of 182!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your credentials!",
          });
        }
      });

    //localStorage.setItem("email", email);

    // setUser({
    //   email: localStorage.getItem("email"),
    // });

    setEmail("");
    setPassword("");
    // alert(email + " has been verified. Welcome back!");
  }

  const retrieveUserDetails = (token) => {
    fetch("http://localhost:4000/users/getUserDetails", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUser({
          _id: data._id,
          isAdmin: data.isAdmin,
          //_id will tell the user logged in, verification of who is loggedin
          //isAdmin to determine which features to show
        });
      });
  };

  return user.id !== null ? (
    <Navigate to="/courses/" />
  ) : (
    <div>
      <h1 className="mt-4">Login Here</h1>
      <Form onSubmit={(e) => logInUser(e)}>
        <Form.Group controlId="userEmail1">
          <Form.Label className="mt-2">Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password1">
          <Form.Label className="mt-2">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        <p>
          Not yet registered? <Link to="/register">Register Here</Link>
        </p>

        {isActive ? (
          <Button
            variant="success"
            type="submit"
            id="submitBTN1"
            className="mt-3 mb-5"
            // onClick={loginSuccess}
          >
            Submit
          </Button>
        ) : (
          <Button
            variant="danger"
            type="submit"
            id="submitBTN1"
            className="mt-3 mb-5"
            disabled
          >
            Submit
          </Button>
        )}
      </Form>
    </div>
  );
}
