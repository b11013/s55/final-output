//import coursesData from "../data/coursesData";
import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";

export default function Courses() {
  //   console.log(coursesData);
  //   console.log(coursesData[0]);

  // const courses = coursesData.map((course) => {
  //   return <CourseCard key={course.id} props={course} />;
  //   //key identifies what elements inside map, for individuality
  // });

  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch("http://localhost:4000/courses")
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setCourses(
          res.map((course) => {
            console.log(course);
            return <CourseCard key={course._id} props={course} />;
          })
        );
      });
  }, []);

  return (
    <div>
      <h1>Courses Available: </h1>
      {courses}
    </div>
  );
}
